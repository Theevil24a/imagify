var express = require('express')
var cors = require('cors');
var app = express();

const { image_search_generator } = require('./custom_libs/duckduckgo-images-api')

app.use(cors({
    origin: '*'
}));

app.use(express.static('public'))

app.post('/', function (req, res) {
    main(req.query.q).catch(console.log)
    .then(data => {
        res.send(data)
        console.log('send to request client')
    })
});

app.listen(5000, () => {
    console.log('run in 5000...')
})


async function main(query) {
    let list = []
    for await (let resultSet of image_search_generator({query: `${query}`.replace(/s/, "+"), moderate: true, iterations: 4 })) {
        list.push(resultSet)
    }

    //list[0].map( item => console.log(item['image']))
    return list
}